# Youtube pwa для ОС Аврора

Одностраничное приложение youtube для Аврора ОС.

The source code of the project is provided under
[the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

## Project Structure

The project has a common structure
of an application based on C++ and QML for Aurora OS.

* **[com.gitlab.danyok.ytpwa.pro](com.gitlab.danyok.ytpwa.pro)** file
  describes the project structure for the qmake build system.
* **[icons](icons)** directory contains application icons for different screen resolutions.
* **[qml](qml)** directory contains the QML source code and the UI resources.
  * **[cover](qml/cover)** directory contains the application cover implementations.
  * **[icons](qml/icons)** directory contains the custom UI icons.
  * **[pages](qml/pages)** directory contains the application pages.
  * **[ytpwa.qml](qml/ytpwa.qml)** file
    provides the application window implementation.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  **[com.gitlab.danyok.ytpwa.spec](rpm/com.gitlab.danyok.ytpwa.spec)** file is used by rpmbuild tool.
  It is generated from **[com.gitlab.danyok.ytpwa.yaml](rpm/com.gitlab.danyok.ytpwa.yaml)** file.
* **[src](src)** directory contains the C++ source code.
  * **[main.cpp](src/main.cpp)** file is the application entry point.
* **[translations](translations)** directory contains the UI translation files.
* **[com.gitlab.danyok.ytpwa.desktop](com.gitlab.danyok.ytpwa.desktop)** file
  defines the display and parameters for launching the application.